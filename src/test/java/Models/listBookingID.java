package Models;

public class listBookingID {
    public int bookingid;
    public listBookingID() {
    }

    public listBookingID(int bookingid) {
        this.bookingid = bookingid;
    }

    public int getBookingid() {
        return bookingid;
    }

    public void setBookingid(int bookingid) {
        this.bookingid = bookingid;
    }
}
