package Models;

public class BookingID_JSONConstructor {
    int bookingid;

    public BookingID_JSONConstructor(int bookingid) {
        this.bookingid = bookingid;
    }
    public BookingID_JSONConstructor() {
    }

    public void setBookingid(int bookingid) {
        this.bookingid = bookingid;
    }

    public int getBookingid() {
        return bookingid;
    }

}
