package Models;

public class Tokens {
    public Tokens() {
    }

    public Tokens(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
    public String getUsername(){
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public  String username;
    public String password;
    public String token;

    public Tokens(String token){
        this.token = token;
    }
    public void setToken(String token){
        this.token = token;
    }
    public String getToken(){
        return token;
    }
}
