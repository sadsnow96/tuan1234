package Models;

public class getBookingID {
    public String firstname;
    public String lastname;
    public String checkin;
    public String checkout;
    public getBookingID() {
    }

    public String getFirstname() {
        return firstname;
    }

    public getBookingID(String firstname, String lastname, String checkin, String checkout) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.checkin = checkin;
        this.checkout = checkout;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }
}
