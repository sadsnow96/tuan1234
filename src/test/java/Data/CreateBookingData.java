package Data;

import Models.BookingDate;
import Models.CreateBooking;

public class CreateBookingData {
    public Object getData(){
        CreateBooking createBooking=new CreateBooking();

        BookingDate bookingDate=new BookingDate();

        createBooking.setFirstname("ABC");
        createBooking.setLastname("XYZ");
        createBooking.setTotalprice(1111);
        createBooking.setDepositpaid(true);
        createBooking.setBookingdates(bookingDate);
        bookingDate.setCheckIn("2020-01-01");
        bookingDate.setCheckOut("2021-01-01");
        createBooking.setAdditionalneeds("Breakfast");
        return createBooking;

    }
}
