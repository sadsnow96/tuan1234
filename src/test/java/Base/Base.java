package Base;

import Data.CreateBookingData;
import Data.TokenData;
import Data.patchData;
import Models.BookingID_JSONConstructor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class Base {
    public List convertJStoList(String url) {
        Response res = given()
                .contentType(ContentType.JSON)
                .when()
                .get(url);
        String JS = res.asString();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<BookingID_JSONConstructor>>() {
        }.getType();
        List<BookingID_JSONConstructor> list = gson.fromJson(JS, listType);
        return list;
    }

    public List convertJStolistID(String url) {
        Response res = given()
                .contentType(ContentType.JSON)
                .when()
                .get(url);
        String JS = res.asString();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<BookingID_JSONConstructor>>() {
        }.getType();
        List<String> listID = new ArrayList<>();
        List<BookingID_JSONConstructor> list = gson.fromJson(JS, listType);
        for (int i = 0; i < list.size(); i++) {
            String bookingid_in_list = null;
            String getBookingid = gson.toJson(list.get(i));
            int limit = getBookingid.length() - 1;
            bookingid_in_list = getBookingid.substring(13, limit);
            listID.add(bookingid_in_list);
        }
        System.out.println("list ID: " + listID);
        return listID;
    }

    public String getToken(String url) {
        Object obj;
        TokenData data = new TokenData();
        obj = data.getData();
        Response res = RestAssured.given()
                .header("Content-Type", "application/json")
                .when()
                .body(obj)
                .post(url);
        String token = res.jsonPath().get("token");//lay token ra
        return token;
    }

    public int create_Booking_and_getID(String url) {
        CreateBookingData data = new CreateBookingData();
        Object obj;
        obj = data.getData();
        Response res = RestAssured.given()
                .header("Content-Type", "application/json")
                .when()
                .body(obj)
                .post(url);

        System.out.print("booking info=>>>>>");
        res.prettyPrint();

        int bookingid = res.jsonPath().get("bookingid");
        System.out.println("Status code create booking: " + res.getStatusCode());
        if(res.getStatusCode()==200){
            System.out.println("<create booking is succeeded>");
        } else {
            System.out.println("<create booking is failed>");
        }
        return bookingid;
    }

    public void check_create_Booking(String url_booking, int bookingid) {
        Gson gson = new Gson();
        int checker = 0;
        String bookingid_in_list = null;
        System.out.println("bookingid has just created: " + bookingid);
        List bookingidList = convertJStoList(url_booking);
        for (int i = 0; i < bookingidList.size(); i++) {
            String getBookingid = gson.toJson(bookingidList.get(i));
            int limit = getBookingid.length() - 1;
            bookingid_in_list = getBookingid.substring(13, limit);
            int bkid = Integer.parseInt(bookingid_in_list);
            if (bkid == bookingid) {
                System.out.println("bookingid find in list: " + bookingid_in_list);
                checker++;
            }
        }
        if (checker > 0) {
            System.out.println("<create booking is succeeded>");
        } else {
            System.out.println("<create booking is failed>");
        }
    }

    public void patchFirstName(String randomID, String token) {
        patchData data = new patchData();
        Object obj = data.getData();

        Response res = given()
                .header("Cookie", "token =" + token)
                .contentType(ContentType.JSON)
                .when()
                .body(obj)
                .patch(randomID);
        res.prettyPrint();
        System.out.println("Status code patch: " + res.getStatusCode());
        if(res.getStatusCode()==200){
            System.out.println("<patch is succeeded>");
        } else {
            System.out.println("<patch is failed>");
        }
    }

    public Response getMethod(String url) {
        Response res = given()
                .contentType(ContentType.JSON)
                .when()
                .get(url);
        return res;
    }

    public String ObjecttoString(Object obj) {
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    public String ResponsetoString(Response response) {
        return response.asString();
    }

    public Response deleteMethod(String url, int bookingid, String token) {
        Response res = RestAssured.given()
                .header("Content-Type", "application/json")
                .header("Cookie", "token=" + token)
                .when()
                .delete(url + bookingid);
        System.out.println("Status code delete: " + res.getStatusCode());
        if(res.getStatusCode()==201){
            System.out.println("<delete is succeeded>");
        } else {
            System.out.println("<delete is failed>");
        }
        return res;
    }
}
