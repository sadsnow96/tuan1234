package api;

import Base.Base;
import Data.patchData;
import org.testng.annotations.Test;
import io.restassured.response.Response;
import java.util.List;
import java.util.Random;

public class Main extends Base {
    String url_booking = "https://restful-booker.herokuapp.com/booking/";
    String url_auth = "https://restful-booker.herokuapp.com/auth/";
    String token = getToken(url_auth);
    int bookingid = create_Booking_and_getID(url_booking);

    @Test(priority = 1)
    public void createBooking() {
        check_create_Booking(url_booking, bookingid);
        System.out.println("==================================END===================================");
    }

    @Test(priority = 2)
    public void patchBooking() {
        List<String> list = convertJStolistID(url_booking);

        Random random = new Random();
        int random_position = random.nextInt(list.size());

        String random_ID = list.get(random_position);
        System.out.println("random_ID = " + random_ID);

        String patchid_url = url_booking + random_ID;
        Response res_before_patch = getMethod(patchid_url);
        String firstname_before_patch = res_before_patch.jsonPath().get("firstname");
        System.out.println("patch link: " + patchid_url);
        patchFirstName(patchid_url, token);
        Response res_after_patch = getMethod(patchid_url);
        String firstname_after_patch = res_after_patch.jsonPath().get("firstname");

        System.out.println("firstname before patch: " + firstname_before_patch);
        System.out.println("firstname after patch: " + firstname_after_patch);

        patchData data = new patchData();
        Object obj = data.getData();
        String firstname = ObjecttoString(obj);

        System.out.println("data provided to patch: " + firstname);
        if (firstname_after_patch != firstname_before_patch && firstname.contains(firstname_after_patch)) {
            System.out.println("patch succeeded");
        } else {
            System.out.println("patch fail");
        }
        System.out.println("==================================END===================================");
    }

    @Test(priority = 7)
    public void deleteBooking() {
        System.out.println("booking id will be delete is: " + bookingid);
        Response delete_Res = deleteMethod(url_booking, bookingid, token);
        if (delete_Res.getStatusCode() == 201) {
            System.out.println("booking id " + bookingid + " is deleted");
        } else {
            System.out.println("delete " + bookingid + " failed");
        }
        System.out.println("==================================END===================================");
    }


}
